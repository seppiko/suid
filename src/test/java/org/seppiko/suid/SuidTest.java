/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.suid;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

/**
 * SUID Test
 *
 * @author Leonard Woo
 */
public class SuidTest {

  private static final Logger logger = LogManager.getLogger();

  @Test
  public void timestampTest() {
    long dayOfMilliSecond = 24 * 60 * 60 * 1000L;
    String dmsStr = Long.toBinaryString(dayOfMilliSecond);
    logger.info("" + dmsStr + " " + dmsStr.length());

    long today = ZonedDateTime.now(Clock.systemUTC()).withYear(1970).withDayOfYear(1).toInstant().toEpochMilli();
    logger.info(Instant.ofEpochMilli(today).atZone(ZoneId.of("UTC")).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
//    String bin = "111111111111111111111111111";
//    logger.info("0x" + Long.toHexString(Long.parseLong(bin, 2)).toUpperCase() + "L");
    long todayOfMilliSecond = today & 0x7FFFFFFL;
    String todayStr = Long.toBinaryString(todayOfMilliSecond);
    logger.info("" + todayStr + " " + todayStr.length());
    logger.info(Instant.ofEpochMilli(todayOfMilliSecond).atZone(ZoneId.of("UTC")).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));

  }

  @Test
  public void binTest() {
    long l = 0L;
    for (int i = 24; i > -1; i--) {
      l += Math.pow(2, i);
    }

    logger.info("" + l);
    logger.info("" + Long.toBinaryString(l));
  }

  @Test
  public void suidTest() {
    SUID suid = SUID.getInstance();
    for (int i = 0; i < 10; i++) {
      long id = suid.nextId();
      logger.info(ZonedDateTime.now(ZoneId.of("UTC")).format(DateTimeFormatter.ISO_OFFSET_TIME));
      logger.info(">> " + + suid.getSequence() + " " + id + " " + String.valueOf(id).length() + " " + Long.toHexString(id).toUpperCase());
      logger.info("------ " + suid.formatId(id));
    }
  }
}
