/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.suid;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Seppiko SUID(Short Unique ID) is a 32bit time-base sequence ID algorithm.
 *
 * +----------------------------------------------------------+
 * | UNUSED(1bit) |   TIMESTAMP(25bit)   |  SEQUENCE-NO(6bit) |
 * +----------------------------------------------------------+
 *
 * @author Leonard Woo
 */
public final class SUID {

  private final long unusedBits = 1L;
  private final long timestampBits = 25L;
  private final long sequenceBits = 6L;

  private final long timestampShift = sequenceBits;

  private final long maxSequence = ~(-1L << sequenceBits); // 2^7-1

  private long sequence = 0L;

  private long lastTimestamp = -1L;

  private static SUID instance;

  /**
   * Initialization instance
   *
   * @return SUID Instance
   */
  public static synchronized SUID getInstance() {
    if (instance == null) {
      instance = new SUID();
    }
    return instance;
  }

  private SUID() {
  }

  private final Lock lock = new ReentrantLock();

  /**
   * get next suid
   *
   * @return SUID
   */
  public long nextId() {
    try {
      lock.lock();

      long currtimestamp = currentTimestamp();

      if (currtimestamp == lastTimestamp) {
        sequence = (sequence + 1) & maxSequence;
        if (sequence == 0) {
          currtimestamp = waitNextMillis(currtimestamp);
        }
      } else {
        sequence = 0L;
      }
      lastTimestamp = currtimestamp;

      long suid = (currtimestamp << timestampShift) |
          sequence;

      return suid;
    } finally {
      lock.unlock();
    }
  }

  /**
   * get the current sequence
   *
   * @return sequence
   */
  public long getSequence() {
    return this.sequence;
  }

  /**
   * current timestamp with offset
   *
   * @return timestamp
   */
  private long currentTimestamp() {
    long today = ZonedDateTime.now(Clock.systemUTC()).withYear(1970).withDayOfYear(1).toInstant().toEpochMilli();
    return today & 0x7FFFFFFL;
  }

  private final AtomicLong waitCount = new AtomicLong(0);

  /**
   * wait next millisecond
   *
   * @param currTimestamp current timestamp
   * @return next timestamp
   */
  private long waitNextMillis(long currTimestamp) {
    waitCount.incrementAndGet();
    while (currTimestamp <= lastTimestamp) {
      currTimestamp = currentTimestamp();
    }
    return currTimestamp;
  }

  /**
   * Format SUID
   *
   * @param id suid
   * @return timestamp and sequence string
   */
  public String formatId(long id) {
    long timestamp = ((id & diode(unusedBits, timestampBits)) >> timestampShift);
    long sequence = (id & diode(unusedBits + timestampBits, sequenceBits));
    String tmf = timeFormat(timestamp);
    return String.format("%s, #%d", tmf, sequence);
  }

  /**
   * time format with ISO offset time
   *
   * @param timestamp timestamp
   * @return format timestamp
   */
  private String timeFormat(long timestamp) {
    return Instant.ofEpochMilli(timestamp).atZone(ZoneId.of("UTC")).format(
        DateTimeFormatter.ISO_OFFSET_TIME);
  }

  /**
   * a diode is a long value whose left and right margin are ZERO, while
   * middle bits are ONE in binary string layout. it looks like a diode in
   * shape.
   *
   * @param offset
   *            left margin position
   * @param length
   *            offset+length is right margin position
   * @return a long value
   */
  private long diode(long offset, long length) {
    int lb = (int) (32 - offset);
    int rb = (int) (32 - (offset + length));
    return (-1L << lb) ^ (-1L << rb);
  }

}
